(function(){
	//this program start with (var map = new Map;) at the bottom of this file


	if (!Array.prototype.shuffle) {
		Array.prototype.shuffle = function() {
			for(var j, x, i = this.length; i; j = parseInt(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x);
				return this;
		};
	}//method to shuffle array. 

	var size = [20,20];
	var bombn = 10;
	var blocks = [];


	function Map (size, bombn) {//bombn for Number of Bomb
		this.init();
	}

	Map.prototype = {
		gamestart:0,//if first clicked, this is 1
		sizex:size[0],
		sizey:size[1],
		init:function(){
			$("<div id='map'></div>").appendTo("body");

			var x = 0;
			var y = 0;
			for(var i=0;i<this.sizex;i++){
				blocks[i] = [];
			};
			for(var i=0;i<(this.sizex*this.sizey);i++){
				var name = x+"_"+y;
				blocks[x][y] = new Block([x,y]);
				if(x == (this.sizey-1)){
					x = 0;
					y++;
				}
				else{
					x++
				}
			};
			this.putBomb();//this should be triggered on click, but for test this is temporarely here.
		},
		putBomb:function(){
			var n = new Array();
			for(var i=0;i<(this.sizex*this.sizey);i++){
				n[i]=i
			}
			n.shuffle();
			n = n.slice(0,bombn);
			var x = 0;
			var y = 0;
			for(var i=0;i<n.length;i++){
				x = n[i]%size[0]-1;
				if(x<0) x = x + size[0];
				y = parseInt(n[i]/size[1]);
				blocks[x][y].bomb = 1;
				blocks[x][y].bombCount = -1;
			}
			this.writeNum();
		},
		writeNum:function(){
			var x=0;
			var y=0;
			for(var i=0;i<(this.sizex*this.sizey);i++){
				// console.log("x"+x+"y"+y+'i'+i);
				blocks[x][y].writeNum();
				if(x == (size[1]-1)){
					x = 0;
					y++;
				}
				else{
					x++
				}
			}
		},
	  // body...
	}

	function Block(position){
		this.position = position;
		this.init();
	}

	Block.prototype = {
		dom:Object(),//
		bomb:0,
		bombCount:0,
		position:[],
		init:function(){
			this.x = this.position[0];
			this.y = this.position[1]; 
			var x = this.x;
			var y = this.y;
			this.dom = $("<div id='"+x+"_"+y+"'></div>").addClass("block");
			$('#map').append(this.dom);
			if(x == (size[0]-1)){
				$(this.dom).after("<br>")
			}

		},
		writeNum:function(){
			if(!this.bomb){
				// var nl,nr,nt,nb = 0;//noleft/right/top/buttom
				var matrix = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]];
				switch(this.x){
					case 0:
						// nl = 1;
						for(var i=(matrix.length-1);i>-1;i--){
							if(matrix[i][0]==-1){matrix.splice(i,1)}
						}
						break;
					case (size[0]-1):
						// nr = 1;
						for(var i=(matrix.length-1);i>-1;i--){
							if(matrix[i][0]==1){matrix.splice(i,1)}
						}
						break;
				}
				switch(this.y){
					case 0:
						// nt = 1;
						for(var i=(matrix.length-1);i>-1;i--){
							if(matrix[i][1]==-1){matrix.splice(i,1)}
						}
						break;
					case (size[1]-1):
						// nb = 1;
						for(var i=(matrix.length-1);i>-1;i--){
							if(matrix[i][1]==1){matrix.splice(i,1)}
						}
						break;
				}
				for(var i=0;i<matrix.length;i++){
					if(blocks[matrix[i][0]+this.x][matrix[i][1]+this.y].bomb){
						this.bombCount++;
					}
				}	
			}
			// console.log(this.bombCount);
			this.dom.html(this.bombCount);
		},
		check:function(){

		},
	  // body...
	}

	var map = new Map();
})()
